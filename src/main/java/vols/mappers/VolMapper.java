package vols.mappers;

import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;
import vols.dto.VolDto;
import vols.entities.Vol;

@Mapper(
        withIoC = IoC.SPRING,
        withIgnoreMissing = IgnoreMissing.ALL,
        withCustomFields = {
                @Field({"Vol.pilote.prenom", "pilote"}),
                @Field({"Vol.copilote.prenom", "copilote"})
        })
public interface VolMapper {

    VolDto asVolDto(Vol v);

    Vol asVol(VolDto vd);

}
