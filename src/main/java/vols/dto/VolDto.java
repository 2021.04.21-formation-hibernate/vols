package vols.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(exclude = {"passagers", "stewards"})
public class VolDto {
    private int id;
    private int num;
    private String pilote;
    private String copilote;

}
