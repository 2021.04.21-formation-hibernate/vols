package vols.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import vols.entities.Marque;
import vols.entities.Vol;

import java.util.List;


public interface VolRepository extends JpaRepository<Vol, Integer> {

    /**
     * Return all vols departing from an airport whose city contains str
     * @param str the string contained in the departure airport city name
     * @return The vols.
     */
    @Query("from Vol where depart.ville like %?1%")
    List<Vol> findByDepartVilleContaining(@Param("ville") String str);

    @Query("from Vol v where v.depart.ville = ?1 and v.arrivee.ville = ?2")
    List<Vol> findByDepartVilleAndArriveeVille(String depart, String arrivee);

    @Query("from Vol v where v.vehicule.marque = ?1 and v.pilote.licence like %?2%")
    List<Vol> findByVehiculeMarqueAndPiloteLicenceContains(Marque marque, String licence);

}
