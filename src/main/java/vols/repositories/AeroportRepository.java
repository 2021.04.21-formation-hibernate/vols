package vols.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vols.entities.Aeroport;
import vols.entities.Vol;

import java.util.List;


public interface AeroportRepository extends JpaRepository<Aeroport, Integer> {

    List<Aeroport> findByVilleContainingOrderByVilleAsc(String ville);
}
