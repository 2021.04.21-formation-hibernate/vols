package vols.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vols.entities.Aeroport;
import vols.entities.Marque;
import vols.entities.Pilote;

import java.util.List;


public interface PiloteRepository extends JpaRepository<Pilote, Integer> {

    @Query("select distinct p from Pilote p join p.volsPilotes v where v.vehicule.marque in :marques")
    List<Pilote> findByVolOnVehiculeOfMarque(@Param("marques") Marque[] marques);
}
