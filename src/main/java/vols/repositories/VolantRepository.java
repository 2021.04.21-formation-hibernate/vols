package vols.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vols.entities.Aeroport;
import vols.entities.Volant;


public interface VolantRepository extends JpaRepository<Volant, Integer> {
}
