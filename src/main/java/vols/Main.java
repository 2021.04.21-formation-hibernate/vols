package vols;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;
import vols.entities.*;
import vols.repositories.AeroportRepository;
import vols.repositories.PiloteRepository;
import vols.repositories.VolRepository;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class Main {

    public static void main(String [] args) {
        SpringApplication.run(Main.class, args);
    }

    public static List<Volant> findVolantByAeroportDeDepartJpql(EntityManager em, String ville) {
        return em.createQuery("select v from Volant v join v.vols vo where vo.depart.ville like :ville")
                .setParameter("ville", "%"+ville+"%")
                .getResultList();
    }

    public static List<Volant> findVolantByAeroportDeDepartSql(EntityManager em, String ville) {
        return em.createNativeQuery(
                "select v.id id, v.marque marque, v.model model, v.dtype DTYPE, v.nbailes nbAiles from volant v " +
                                "inner join vol vo on vo.vehicule_id = v.id " +
                                "inner join aeroport a on vo.depart_id = a.id " +
                                "where a.ville like :ville", Volant.class)
                .setParameter("ville", "%" + ville + "%")
                .getResultList();
    }


    @Bean
    public CommandLineRunner console(VolRepository vr, AeroportRepository ar, EntityManager em, PiloteRepository pr) {
        return (args) -> {
//            // creating cities name array
//            String [] cities = {"Brest", "Nice", "Lyon", "London", "Berlin", "New York"};
//            // creating airports array
//            List<Aeroport> aeroports = Arrays.stream(cities)
//                    .map(c -> new Aeroport().setVille(c))
//                    .peek(a -> ar.save(a))
//                    .collect(Collectors.toList());
//            // creating vols
//            for (int i=0; i<10; i++) {
//                vr.save(new Vol()
//                    .setDepart(aeroports.get((int)(Math.random()*aeroports.size())))
//                    .setArrivee(aeroports.get((int)(Math.random()*aeroports.size()))));
//            }

            //vr.findByDepartVilleContaining("on").forEach(System.out::println);
//            System.out.println("> JPQL :");
//            findVolantByAeroportDeDepartJpql(em, "on").forEach(s -> System.out.println("\t" + s));
//            System.out.println("> SQL :");
//            findVolantByAeroportDeDepartSql(em, "on").forEach(s -> System.out.println("\t" + s));
//
//            Pilote [] ps = {
//                    pr.save((Pilote) new Pilote().setLicence("tout appareil").setPrenom("Ashraf")),
//                    pr.save((Pilote) new Pilote().setLicence("tout appareil").setPrenom("Lionel")),
//                    pr.save((Pilote) new Pilote().setLicence("tout appareil").setPrenom("Ziyaad"))
//            };
//            Arrays.stream(ps).forEach(pr::save);
//
//            vr.findAll().forEach(v -> {
//                v.setPilote(ps[(int)(Math.random()*ps.length)]);
//                vr.save(v);
//            });
//
//
//            System.out.println("> Pilotes par marque :");
//            pr.findByVolOnVehiculeOfMarque(new Marque[] {Marque.AIRBUS, Marque.BOEING}).forEach(p -> System.out.println("\t" + p));


        };
    }

}
