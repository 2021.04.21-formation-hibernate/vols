package vols.entities;

public enum Marque {
    BOEING,
    AIRBUS,
    DASSAULT,
    ANTONOV
}
