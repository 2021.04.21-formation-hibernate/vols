package vols.entities;

import lombok.*;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Helicoptere extends Volant {

    private int nbHelices;
    
}
