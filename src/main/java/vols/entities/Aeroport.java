package vols.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(exclude = {"departs", "arrivees"})

@Entity

public class Aeroport {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private int id;

    private String ville;

    @OneToMany(mappedBy = "depart")
    private Set<Vol> departs;

    @OneToMany(mappedBy = "arrivee")
    private Set<Vol> arrivees;
}
