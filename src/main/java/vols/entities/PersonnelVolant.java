package vols.entities;

import lombok.*;

import javax.persistence.Entity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Entity
public abstract class PersonnelVolant extends Personne {

    private int numEmploye;

}
