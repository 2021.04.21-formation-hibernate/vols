package vols.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = {"volsPilotes", "volsCopilotes"}, callSuper = true)
@Entity
public class Pilote extends PersonnelVolant {

    private String licence;

    @OneToMany(mappedBy = "pilote")
    private Set<Vol> volsPilotes = new HashSet<>();

    @OneToMany(mappedBy = "copilote")
    private Set<Vol> volsCopilotes = new HashSet<>();

}
