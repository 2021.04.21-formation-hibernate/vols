package vols.entities;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personne {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private int id;

    private String nom;

    private String prenom;

}
