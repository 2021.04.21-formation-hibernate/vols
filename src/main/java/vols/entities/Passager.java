package vols.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = {"vols"}, callSuper = true)
@Entity
public class Passager extends Personne {

    @ManyToMany(mappedBy = "passagers")
    private Set<Vol> vols = new HashSet<>();

    @OneToOne
    private Passeport passeport;

}
