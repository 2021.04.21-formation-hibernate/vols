package vols.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Passeport {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private int id;

    private int num;

}
