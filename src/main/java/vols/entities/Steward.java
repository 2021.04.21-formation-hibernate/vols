package vols.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Steward extends PersonnelVolant {

    private String grade;

    @ManyToMany(mappedBy = "stewards")
    private Set<Vol> vols = new HashSet<>();

}
