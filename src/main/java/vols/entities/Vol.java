package vols.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(exclude = {"passagers", "stewards"})

@Entity

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Vol {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private int id;

    @NotNull
    @Min(0)
    private int num;

    @ManyToOne
    private Pilote pilote;

    @ManyToOne
    private Pilote copilote;

    @ManyToMany
    @JoinTable(name = "vol_steward")
    private Set<Steward> stewards = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "vol_passager")
    private Set<Passager> passagers = new HashSet<>();

    @ManyToOne
    private Aeroport depart;

    @ManyToOne
    private Aeroport arrivee;

    @ManyToOne
    private Volant vehicule;
}
