package vols.entities;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Volant {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private int id;

    private Marque marque;

    private String model;

    @OneToMany(mappedBy = "vehicule")
    private Set<Vol> vols = new HashSet<>();
}
