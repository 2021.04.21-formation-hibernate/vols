package vols.controllers;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vols.dto.VolDto;
import vols.entities.Vol;
import vols.mappers.VolMapper;
import vols.repositories.VolRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vols")
public class VolController {
    // vols, GET => findAll
    // vols/{id}, GET => findById
    // vols, POST => save
    // vols/{id}, PUSH => update
    // vols/{id}, DELETE => delete

    private VolRepository vr;

    private VolMapper vm;

    public VolController(
            VolRepository vr,
            VolMapper vm) {
        this.vr = vr;
        this.vm = vm;
    }

    @GetMapping
    public List<VolDto> findAll() {
        return vr.findAll().stream()
                .map(v -> vm.asVolDto(v))
                .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public Vol findById(@PathVariable("id") int id) {
        return vr.findById(id).orElseThrow();
    }

    @PostMapping
    public void save(@Validated @RequestBody Vol v) {
        vr.save(v);
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") int id, @Validated @RequestBody Vol v) {
        v.setId(id);
        vr.save(v);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") int id) {
        vr.deleteById(id);
    }

}
