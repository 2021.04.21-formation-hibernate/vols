package vols.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vols.entities.Aeroport;
import vols.entities.Vol;
import vols.repositories.AeroportRepository;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/aeroports-manuel")
public class AeroportController {

    @Autowired
    private AeroportRepository ar;

    @GetMapping
    public List<Aeroport> findAll() {
        return ar.findAll();
    }

    @GetMapping("{id}")
    public Aeroport findById(@PathVariable("id") int id) {
        return ar.findById(id).orElseThrow();
    }

    @GetMapping("{id}/departs")
    public Set<Vol> getDepart(@PathVariable("id") int id) {
        return ar.findById(id).orElseThrow().getDeparts();
    }

    @PostMapping
    public Aeroport save(@RequestBody Aeroport a) {
        return ar.save(a);
    }

    @PutMapping("{id}")
    public Aeroport update(@PathVariable("id") int id, @RequestBody Aeroport a) {
        a.setId(id);
        return ar.save(a);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") int id) {
        ar.deleteById(id);
    }
}
